#!/bin/sh
# shellcheck shell=sh
# -------------------------------------------------------------------------------------
# Summary: Initialize the terraform project locally.
# Detail:
#   Before running this script, perform the following steps:
#     - install terraform
#     - create a personal access token with the API scope
#       - https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
#     - export the variables `GL_USERNAME` and `GL_TOKEN` into the active shell
#   Then run this script: ./terraform-init.sh
# -------------------------------------------------------------------------------------

raise_error() {
  # display the given error message and exit
  #
  #   Args:
  #       $1                      the message (no color)
  #       $2                      if provided, print this in red on the next line, too

  printf '\033[1;31m[ ERROR ]\033[0m %s' "$1"
  if [ -n "$2" ]; then
    printf '\033[1;31m %s\033[0m' "$2"
  fi
  echo && exit 1
}

if ! command -v terraform >/dev/null; then
  raise_error "missing command:" "terraform"
fi

if [ -z "$GL_USERNAME" ]; then
  raise_error "missing environment variable:" "GL_USERNAME"
fi

if [ -z "$GL_TOKEN" ]; then
  raise_error "missing environment variable:" "GL_TOKEN"
fi

test -z "$TF_STATE_NAME" && TF_STATE_NAME="${AWS_PROFILE:-default}"

gl_project_id="${GL_PROJECT_ID:-33138820}"

terraform init \
  -backend-config="address=https://gitlab.com/api/v4/projects/$gl_project_id/terraform/state/$TF_STATE_NAME" \
  -backend-config="lock_address=https://gitlab.com/api/v4/projects/$gl_project_id/terraform/state/$TF_STATE_NAME/lock" \
  -backend-config="unlock_address=https://gitlab.com/api/v4/projects/$gl_project_id/terraform/state/$TF_STATE_NAME/lock" \
  -backend-config="username=$GL_USERNAME" \
  -backend-config="password=$GL_TOKEN" \
  -backend-config="lock_method=POST" \
  -backend-config="unlock_method=DELETE" \
  -backend-config="retry_wait_min=5" "$@"
