# Summary: Define and configure the provider backend to use with this Terraform module.
# Author:  Bryant Finney  (https://bryant-finney.github.io/about/)

# use the hashicorp/aws provider
provider "aws" {
  region = "us-east-1"
  default_tags {
    tags = local.tags
  }
}

# Shared location for terraform state files
# ref: https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html
terraform {
  backend "http" {
  }
}

# current aws account.
# access account_id, arn via data.aws_caller_identity.current.account_id
data "aws_caller_identity" "current" {}

locals {
  aws_region = "us-east-1"
}
